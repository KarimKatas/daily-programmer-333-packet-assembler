<?php

namespace Kata;

class Packet
{
    private $messageId;
    private $count;
    private $total;
    private $content;

    public function __construct(int $messageId, int $count, int $total, ?string $content)
    {
        $this->messageId = $messageId;
        $this->count = $count;
        $this->total = $total;
        $this->content = $content;
    }

    public function messageId(): int
    {
        return $this->messageId;
    }

    public function count(): int
    {
        return $this->count;
    }

    public function total(): int
    {
        return $this->total;
    }

    public function content(): ?string
    {
        return $this->content;
    }

    public function format(): string
    {
        return sprintf("%-8u%-4u%-4u%s",
            $this->messageId(),
            $this->count(),
            $this->total(),
            $this->content()
        );
    }
}