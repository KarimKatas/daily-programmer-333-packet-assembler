<?php

namespace Kata;

use LogicException;

class Message
{
    /** @var array|Packet[] */
    private $stack = [];
    private $total = null;

    public function add(Packet $packet): void
    {
        $this->stack[$packet->count()] = $packet;

        if (!$this->total) {
            $this->total = $packet->total();
        }
    }

    public function hasAllPackagesReceived(): bool
    {
        return $this->total === count($this->stack);
    }

    public function content(): ?string
    {
        if (!$this->hasAllPackagesReceived()) {
            throw new LogicException('Message incomplete. Not all packages received.');
        }

        ksort($this->stack);

        return implode("\n", array_map(function (Packet $packet) {
            return $packet->format();
        }, $this->stack));
    }

    public function id(): int
    {
        return reset($this->stack)->messageId();
    }
}