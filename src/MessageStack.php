<?php

namespace Kata;

class MessageStack
{
    /** @var array|Message[] */
    private $stack = [];

    public function findOrCreateMessageFor(Packet $packet): Message
    {
        $message = $this->stack[$packet->messageId()] ?? new Message;

        return $this->stack[$packet->messageId()] = $message;
    }

    public function remove(Message $message): void
    {
        unset($this->stack[$message->id()]);
    }
}