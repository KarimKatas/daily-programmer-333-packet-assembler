<?php

namespace Kata;

class Application
{
    /** @var resource */
    private $inputStream;

    /** @var resource */
    private $outputStream;

    /** @var MessageStack */
    private $messages;

    public function __construct($inputStream, $outputStream)
    {
        $this->inputStream = $inputStream;
        $this->outputStream = $outputStream;
        $this->messages = new MessageStack;
    }

    public function run(): void
    {
        while ($packet = $this->parsePacket(fgets($this->inputStream))) {
            $message = $this->messages->findOrCreateMessageFor($packet);
            $message->add($packet);
            if ($message->hasAllPackagesReceived()) {
                $this->print($message);
                $this->messages->remove($message);
            }
        }
    }

    private function parsePacket(?string $line): ?Packet
    {
        preg_match('/^([\d ]{8})([\d ]{4})([\d ]{4})(.*)$/', $line, $matches);

        if (!$matches) {
            return null;
        }

        return new Packet((int)$matches[1], (int)$matches[2], (int)$matches[3], $matches[4]);
    }

    private function print(Message $message): void
    {
        fwrite($this->outputStream, $message->content() . "\n");
    }
}