<?php

namespace Tests;

use Kata\Application;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function testWorkflowWithChallengeInput()
    {
        $this->runFor(__DIR__ . '/data/challenge_input.txt', __DIR__ . '/data/challenge_output.txt');
    }

    public function testWorkflowWithSampleInput()
    {
        $this->runFor(__DIR__ . '/data/sample_input.txt', __DIR__ . '/data/sample_output.txt');
    }

    private function runFor($inputFile, $outputFile): void
    {
        $output = fopen('php://memory', 'rw');

        $application = new Application(fopen($inputFile, 'r'), $output);

        $application->run();

        rewind($output);

        $this->assertEquals(file_get_contents($outputFile), stream_get_contents($output));
    }
}