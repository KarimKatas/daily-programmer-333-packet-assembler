<?php

namespace Tests;

use Kata\Application;
use Kata\Message;
use Kata\Packet;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{
    public function testAddingPackages()
    {
        $message = new Message;

        $this->assertFalse($message->hasAllPackagesReceived());

        $message->add(new Packet(1, 0, 2, 'Foo'));
        $this->assertFalse($message->hasAllPackagesReceived());

        $message->add(new Packet(1, 1, 2, 'Bar'));
        $this->assertTrue($message->hasAllPackagesReceived());
    }

    public function testContentFormattingWithSpaces()
    {
        $message = new Message;
        $message->add(new Packet(1, 0, 2, 'Foo '));
        $message->add(new Packet(1, 1, 2, ' Bar'));

        $this->assertEquals("1       0   2   Foo \n1       1   2    Bar", $message->content());
    }
}